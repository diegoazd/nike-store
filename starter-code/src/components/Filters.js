import React, { Component } from "react";

import "./Filters.css";

export default class Filters extends Component {
  render() {
    return (
      <aside className="Section">
        <h3 className="Section-Title">Filters</h3>
        <div className="Block">
          <h4 className="Block-Title">Shop By Price</h4>
          <label className="Block-Option">
            <input name="price" type="checkbox" defaultChecked={false} value="cheap" onClick={this.props.filterByPrice}/> Under $50
          </label>
          <label className="Block-Option">
            <input name="price" type="checkbox" defaultChecked={false} value="regular" onClick={this.props.filterByPrice}/> $50 to $150
          </label>
          <label className="Block-Option">
            <input name="price" type="checkbox" defaultChecked={false} value="expensive" onClick={this.props.filterByPrice}/> Up to $150
          </label>
        </div>


        <div className="Block">
          <h4 className="Block-Title">Sport</h4>
          <label className="Block-Option">
            <input name="sport" type="checkbox" value="lifestyle" onClick={this.props.filterByCategory} /> Lifestyle
          </label>
          <label className="Block-Option">
            <input name="sport" type="checkbox" value="running" onClick={this.props.filterByCategory} /> Running
          </label>

          <label className="Block-Option">
            <input name="sport" type="checkbox" value="basketball" onClick={this.props.filterByCategory} /> Basketball
          </label>

          <label className="Block-Option">
            <input name="sport" type="checkbox" value="soccer" onClick={this.props.filterByCategory} /> Soccer
          </label>

          <label className="Block-Option">
            <input name="sport" type="checkbox" value="training" onClick={this.props.filterByCategory}/> Training & Gym
          </label>
        </div>

        <div className="Block">
          <h4 className="Block-Title">Closure Type</h4>
          <label className="Block-Option">
            <input name="type" type="checkbox" value="click" onClick={this.props.filterByType} /> Click
          </label>

          <label className="Block-Option">
            <input name="type" type="checkbox" value="slipon" onClick={this.props.filterByType}/> Slip
          </label>

           <label className="Block-Option">
            <input name="type" type="checkbox" value="strap" onClick={this.props.filterByType}/> On Strap
          </label>
        </div>

        <div className="Block">
          <h4 className="Block-Title">Color</h4>
          <button type="color" className="Block-Color" data-color="white" value="white" onClick={this.props.filterByColor}></button>
          <button type="color" className="Block-Color" data-color="silver" value="silver" onClick={this.props.filterByColor}></button>
          <button type="color" className="Block-Color" data-color="yellow" value="yellow" onClick={this.props.filterByColor}></button>
          <button type="color" className="Block-Color" data-color="gold" value="gold" onClick={this.props.filterByColor}></button>
          <button type="color" className="Block-Color" data-color="orange" value="orange" onClick={this.props.filterByColor}></button>
          <button type="color" className="Block-Color" data-color="green" value="green" onClick={this.props.filterByColor}></button>
          <button type="color" className="Block-Color" data-color="blue" value="blue" onClick={this.props.filterByColor}></button>
          <button type="color" className="Block-Color" data-color="pink" value="pink" onClick={this.props.filterByColor}></button>
          <button type="color" className="Block-Color" data-color="olive" value="olive" onClick={this.props.filterByColor}></button>
          <button type="color" className="Block-Color" data-color="red" value="red" onClick={this.props.filterByColor}></button>
          <button type="color" className="Block-Color" data-color="purple" value="purple" onClick={this.props.filterByColor}></button>
          <button type="color" className="Block-Color" data-color="grey" value="grey" onClick={this.props.filterByColor}></button>
        </div>

        <div className="Block">
          <h4 className="Block-Title">Size</h4>
          <div style={{ display: "flex", flexWrap: "wrap" }}>
            <button className="Block-Size">3</button>
            <button className="Block-Size">3.5</button>
            <button className="Block-Size">4</button>
            <button className="Block-Size">4.5</button>
            <button className="Block-Size">5</button>
            <button className="Block-Size">5.5</button>
            <button className="Block-Size">6</button>
            <button className="Block-Size">6.5</button>
            <button className="Block-Size">7</button>
            <button className="Block-Size">7.5</button>
            <button className="Block-Size">8</button>
            <button className="Block-Size">8.5</button>
            <button className="Block-Size">9</button>
            <button className="Block-Size">9.5</button>
            <button className="Block-Size">10</button>
            <button className="Block-Size">10.5</button>
            <button className="Block-Size">11</button>
            <button className="Block-Size">11.5</button>
            <button className="Block-Size">12</button>
            <button className="Block-Size">12.5</button>
            <button className="Block-Size">13</button>
            <button className="Block-Size">13.5</button>
            <button className="Block-Size">14</button>
            <button className="Block-Size">14.5</button>
            <button className="Block-Size">15</button>
            <button className="Block-Size">15.5</button>
            <button className="Block-Size">16</button>
            <button className="Block-Size">16.5</button>
            <button className="Block-Size">17</button>
            <button className="Block-Size">18</button>
            <button className="Block-Size">19</button>
            <button className="Block-Size">20</button>
            <button className="Block-Size">21</button>
            <button className="Block-Size">22</button>
          </div>
        </div>
      </aside>
    );
  }
}
