import React, { Component } from "react";
import Gallery from './Gallery';

import "./Content.css";

// <div class="Content">
// <h3 class="Content-Title">
//   Men’s Shoes & Skeakers
//   <span class="Results">(876)</span>
// </h3>
// <p class="Results-Description">Explore the latest shoes for men for every sport, workout and everyday look. Built for ultimate performance and sneaker style, Nike shoes for men deliver cutting-edge technologies specific to your sport in iconic designs.</p>
// Gallery--
// </div>

export default class Content extends Component {
  render() {
    return (
<div className="Content">
<h3 className="Content-Title">
  Men’s Shoes & Skeakers
  <span className="Results">({this.props.shoes.length})</span>
</h3>
<p className="Results-Description">Explore the latest shoes for men for every sport, workout and everyday look. Built for ultimate performance and sneaker style, Nike shoes for men deliver cutting-edge technologies specific to your sport in iconic designs.</p>
<Gallery shoes={this.props.shoes}/>
 </div>
    );
  }
}
