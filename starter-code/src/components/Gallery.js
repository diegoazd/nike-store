import React, { Component } from "react";

import "./Gallery.css";

export default class Gallery extends Component {
  render() {
    return (
        <div className="Gallery">
        {
          this.props.shoes.map(shoe => {
              return <div className="Shoe" key={shoe._id}>
                <img src={shoe.url}/>
                <span className="Shoe-Color">{shoe.colors.length} Colors</span>
                <hr />
                <em className="Shoe-Title">${shoe.name}</em>
                <span className="Shoe-Type">${shoe.type}</span>
                <span className="Shoe-Price">${shoe.price}</span>
              </div>
          })
        }
            </div>
    );
  }
}
