import React, { Component } from 'react';
import Header from './components/Header'
import Grid from './components/Grid'
import Filters from './components/Filters'
import Content from './components/Content'
import axios from 'axios';


class App extends Component {

  constructor() {
    super();
    this.state = {shoes: [], filters: {price: '', sport: '', type: '', color: ''}
    };
    /*
    this.state = {shoes: [], filters: {price: {cheap: false, regular: false, expensive: false}, 
      sport: {lifestyle: false, running: false, basketball: false, soccer: false, training: false}, 
      type: {click: false, slipon: false, strap: false}}
    };*/
  }

  filterByPrice = (e) => {
    const filters = this.state.filters;
    this.toogleElementAndSetFalseOthers(filters, e.target.value, 'price')
    this.setState({filters:filters});

    this.handleCheckElements("price", e.target.value, filters['price']);
    axios.get(`https://nikeapi.herokuapp.com/api/v1/shoes${this.getQuery(filters)}`).then(r => this.setState({shoes: r.data.data}));
  }

  filterByCategory = (e) => {
    const filters = this.state.filters;
    this.toogleElementAndSetFalseOthers(filters, e.target.value, 'sport')
    this.setState({filters:filters});

    this.handleCheckElements("sport", e.target.value, filters['sport']);
    axios.get(`https://nikeapi.herokuapp.com/api/v1/shoes${this.getQuery(filters)}`).then(r => this.setState({shoes: r.data.data}));
  }

  filterByType = (e) => {
    const filters = this.state.filters;
    this.toogleElementAndSetFalseOthers(filters, e.target.value, 'type')
    this.setState({filters:filters});

    this.handleCheckElements("type", e.target.value, filters['type']);
    axios.get(`https://nikeapi.herokuapp.com/api/v1/shoes${this.getQuery(filters)}`).then(r => this.setState({shoes: r.data.data}));
  }

  filterByColor = (e) => {
    const filters = this.state.filters;
    this.toogleElementAndSetFalseOthers(filters, e.target.value, 'color')
    this.setState({filters:filters});

    this.handleCheckElements("color", e.target.value, filters['color']);
    axios.get(`https://nikeapi.herokuapp.com/api/v1/shoes${this.getQuery(filters)}`).then(r => this.setState({shoes: r.data.data}));
  }

  getQuery = (filters) => {
    let query = [];
    for(var keyCategory in filters) {
      if(filters[keyCategory]) {
        query.push(`${keyCategory}=${filters[keyCategory]}`)
      }
          
    }
    return query ? '?'+query.join("&") : '';
  }

  toogleElementAndSetFalseOthers = (filter, value, element) => {
    if(filter[element] === value) {
      filter[element] = '';
    }else {
      filter[element] = value;
    }
  }

  handleCheckElements = (name, value, currentValue) => {
    document.querySelectorAll(`input[name='${name}']`).forEach( element => {
      if(element.value === value) {
        element.checked = (currentValue);
      }else {
        element.checked = false;
      }
    });
  }

  componentDidMount() {
    axios.get('https://nikeapi.herokuapp.com/api/v1/shoes').then(r => this.setState({shoes: r.data.data}));
  }

  render() {
    return (
      <div> 
          <Header/>
          <Grid>
            <Filters filterByPrice={this.filterByPrice} filterByCategory={this.filterByCategory} filterByType={this.filterByType} 
                filterByColor={this.filterByColor}/>
            <Content shoes={this.state.shoes}/>
          </Grid>
      </div>
   );
  }
}

export default App;
